import random
import string
import zipfile
import io
import pathlib
import subprocess as sp
import os

from datetime import datetime
from flask import Flask, render_template, send_file
from flask_socketio import SocketIO, send, emit

app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'
socketio = SocketIO(app)

@app.route('/')
def index():
    return render_template("index.html")

@app.route('/python/download/<filename>')
def python_download(filename):
    try:
        base_path = pathlib.Path('../python/results/{}'.format(filename))
        data = io.BytesIO()
        with zipfile.ZipFile(data, mode='w') as zfile:
            for f_name in base_path.iterdir():
                zfile.write(f_name, f_name.name, compress_type=zipfile.ZIP_DEFLATED)
        data.seek(0)
        return send_file(
            data,
            mimetype='application/zip',
            as_attachment=True,
            attachment_filename='{}.zip'.format(filename))
    except Exception as err:
        return str(err)

def run_python(jsondata=None):
    """
        jsondata in format:
        { p : pages, s: searchstring}
    """
    command = ["help"]
    print(os.getcwd())
    if (jsondata):
        fname = "{}{}".format(random_string(10), datetime.now().strftime("%Y-%m-%d_%H%M%S"))
        command = ["etsy_webscraper/webscraper.py",
                   "-p", "{p}".format(**jsondata),
                   "-s", "'{s}'".format(**jsondata),
                   "-f", "{}".format(fname)]
    emit('console_response', ' '.join(["python"] + command))
    with sp.Popen(["python"] + command,
                  bufsize=0,
                  stdout=sp.PIPE, stderr=sp.STDOUT,
                  cwd="../python/") as process:
        for line in process.stdout:
            emit('console_response', line.decode('utf-8').strip())

    emit('done', {'filename': fname})
    return ""

def random_string(string_lenth=10):
    """Generate a random string of fixed length """
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(string_lenth))

@socketio.on('run')
def socket_run_python(jsondata):
    run_python(jsondata)

@socketio.on('connect')
def test_connect():
    emit('my response', {'data': 'Connected'})

@socketio.on('disconnect')
def test_disconnect():
    print('Client disconnected')


if __name__ == '__main__':
    socketio.run(app, debug=false)