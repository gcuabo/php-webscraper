<?php
require "getResults.php";
require "threading.php";
require "utils/errorHandling.php";

$STORES_THREADING_SIZE = 50;

$shortopts = "s:";  // Required value
$shortopts .= "p:"; // Required value

$longopts  = array(
    "search_string:",     // search string; Required value
    "pages:",    // pages Required value
);

$options = getopt($shortopts, $longopts);

$PAGES = $options['p'];
$SEARCH_STRING = $options['s'];

print("Fetching $PAGES pages for \"$SEARCH_STRING\"\n");

// Get Item Results
$results = [];  // listing results
$shops = [];    // shop results
$store_links = [];
$stores = [];   

$date_today = getdate();
$folder_name = './' . $date_today['year'] . $date_today['mon'] . $date_today['mday'] . $date_today['hours'] . $date_today['minutes'] . $date_today['seconds'];
mkdir($folder_name);

foreach (range(1, $PAGES) as $page)
{
    print("Fetching page " . $page ." results ....\n");
    $url = "https://www.etsy.com/search?q=" . urlencode($SEARCH_STRING) . "&ref=pagination&page=" . $page;
    $results = multiThread_getItemLinkContents(getEtsyResults($url));

    print("writing to file\n");
    toCSV($folder_name . '/items.csv', $results, 'a+');

    foreach ($results as $r)
    {
        $store_handle = $r->store_handle;
        if(!in_array($store_handle, $store_links))
        {
            $s = [new Store($store_link = "https://www.etsy.com/shop/". $store_handle)];
            toCSV($folder_name . '/stores_tmp_20pages.csv', $s, 'a+');

            array_push($store_links, $store_handle);
        }
    }

}

print("Fetching Store Details...\n");
$file = fopen($folder_name . "/stores_tmp_20pages.csv", "r");
$current_list_of_stores = [];
$count = 1;

while(! feof($file))
{
    $l = fgetcsv($file)[0]; 

    if ($l != "")
    {
        array_push($current_list_of_stores, new Store($store_link = $l));

        if (($count % $stores_threading_size) == 0)
        {
            multiThread_getStoreContents($current_list_of_stores);
            toCSV($folder_name . '/shops.csv', $current_list_of_stores, 'a+');
            $current_list_of_stores = [];
        }
    }
    
    $count++;
}

if (($count - 1) % $STORES_THREADING_SIZE != 0)
{
    multiThread_getStoreContents($current_list_of_stores);
    toCSV($folder_name . '/shops.csv', $current_list_of_stores, 'a+');
}

fclose($file);