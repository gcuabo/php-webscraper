<?php
include "store.class.php";

class Item {
    public $item_name = null;
    public $store = null;
    public $stars_average = null;
    public $number_of_raters = null;
    public $currency_value = null;
    public $link = null;
    public $currency_symbol = null;
    public $on_sale = null;
    public $original_price_value = null;
    public $price = null;
    public $original_price = null;
    public $store_obj = null;
        
    public function __construct(
        $item_name, 
        $store, 
        $stars_average, 
        $number_of_raters,
        $currency_value,
        $link,
        $currency_symbol = "$",
        $on_sale = False, 
        $original_price_value = null) 
    {
        $this->item_name = rtrim(ltrim($item_name));
        $this->store = $store;
        $this->stars_average = $stars_average;
        preg_match('#(\d+(?:(,)*\d*))#', $number_of_raters, $number_of_raters);
        try {
            $this->number_of_raters = $number_of_raters[1];
        } catch (Exception $e){
            $this->number_of_raters = 0;
        }
        $this->currency_value = $currency_value;
        $this->link = $link;
        $this->currency_symbol = $currency_symbol;
        $this->on_sale = $on_sale;
        $this->original_price_value = $original_price_value;

        $this->price = $currency_symbol . ' ' . $currency_value;
        $this->original_price = $currency_symbol . ' ' . $original_price_value;

        $this->store_link = null;
        $this->store_handle = null;
    }

    function getStoreLink($link_content=null)
    {
        /*
            FIND THE URL OF THE STORE
            link_content = content of $this->link
        */

        if (is_null($this->store_link))
        {
            if (is_null($link_content))
            {
                $ch = curl_init();
                curl_setopt_array($ch, [
                    CURLOPT_RETURNTRANSFER => 1,
                    CURLOPT_URL => $this->link // this is the listing link
                ]);
                
                // OPEN LISTING LINK
                $link_content = curl_exec($ch);
                
                curl_close($ch); 
            }

            $doc = new DOMDocument();
            libxml_use_internal_errors(true);
            $doc->loadHTML($link_content);
            
            $class = "text-link-no-underline text-gray-lightest";
            $aTag = findElement($doc, $class, $tag="a", $attr="class");
            $store_link = $aTag->getAttribute("href");
            
            $regex  = '#https://www.etsy\.com/shop/(\w+)#';
            preg_match($regex, $store_link, $store_handle);

            $this->store_link = $store_link;
            $this->store_handle = $store_handle[1];

        } 
        return $this->store_link;
    }

    public function toArray()
    {
        return [
            $this->item_name,
            $this->store,
            $this->stars_average,
            $this->number_of_raters,
            $this->price
        ];
    }
}