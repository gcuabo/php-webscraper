<?php

class Store {
    public function __construct(
        $store_link,
        $handle = null,
        $store_name = null, 
        $description = null, 
        $sales = null, 
        $on_etsy_since = null,
        $star_rating = null,
        $number_of_raters = null,
        $favourites= null,
        $shop_owner = null ) 
    {
        $this->link = $store_link;
        $this->handle = $handle;
        $this->store_name = $store_name;
        $this->description = $description;
        $this->sales = $sales;
        $this->on_etsy_since = $on_etsy_since;
        $this->star_rating = $star_rating;
        $this->number_of_raters = $number_of_raters;
        $this->favourites = $favourites;
        $this->shop_owner = $shop_owner;
    }

    public function getDetails(){
        if (is_null($this->store_name))
        {
            $store_name = null;
            $description = null; 
            $sales = null; 
            $on_etsy_since = null;
            $star_rating = null;
            $number_of_raters = null;
            $favourites = null;
            $ch = curl_init();
            curl_setopt_array($ch, [
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => $this->link 
            ]);

            // OPEN Store LINK
            $link_content = curl_exec($ch);
            
            curl_close($ch); 
        }
        $this->sortDetails($link_content);
    }

    public function sortDetails($content)
    {
        $doc = new DOMDocument();
        libxml_use_internal_errors(true);
        try {
            $doc->loadHTML($content);
        } catch (Exception $e) {
            print_r($content);
        }
        $regex  = '#\"num_favorers\":(\d+(?:(,)*\d*)),#';
        preg_match($regex, $content, $favourites_array);
        try {
            $favourites = $favourites_array[1];
        } catch (Exception $e){
            $favourites = 0;
        }

        $shop_owner_dom = findElement($doc, 'user-avatar', 'div', 'data-editable-img');
        $shop_owner = $shop_owner_dom->nodeValue;

        $shop_info_div = findMatch($doc, 'shop-info');
        $doc->loadHTML($shop_info_div);

        $name_and_title_container = findMatch($doc, 'shop-name-and-title-container');
        $doc_inner = new DOMDocument();
        $doc_inner->loadHTML($name_and_title_container);
        $store_name = findMatch($doc_inner, 'mb-lg-1', 'h1');
        $description = findMatch($doc_inner, 'title', 'span', 'data-in-modal-editable-text'); 
 
        $regex  = '#(\d+(?:(,)*\d*)) Sales#';
        preg_match($regex, $content, $sales_array);
        try {
            $sales = $sales_array[1];
        } catch (Exception $e){
            $sales = 0;
        }

        $on_etsy_since = findMatch($doc, 'etsy-since', 'span');

        preg_match_all("/stars-svg-star/", $shop_info_div, $stars_match);
        $star_rating = count($stars_match[0]);

        $number_of_raters = findMatch($doc, 'total-rating-count', 'span');
        preg_match('#(\d+(?:(,)*\d*))#', $number_of_raters, $number_of_raters);
        preg_match('#(\d+)#', $on_etsy_since, $on_etsy_since);

        $this->shop_owner = rtrim(ltrim($shop_owner));
        $this->store_name = $store_name;
        $this->description = $description;
        $this->sales = $sales;
        $this->on_etsy_since = $on_etsy_since[1];
        $this->star_rating = $star_rating;
        try {
            $this->number_of_raters = $number_of_raters[1];
        } catch (Exception $e){
            $this->number_of_raters = 0;
        }
        $this->favourites = $favourites;
    }

    public function toArray()
    {
        return [
            $this->link, 
            $this->store_name, 
            $this->description, 
            $this->sales, 
            $this->on_etsy_since,
            $this->star_rating,
            $this->number_of_raters,
            $this->favourites,
            $this->shop_owner 
        ];
    }
}