<?php

function multiThread_getItemLinkContents(array $items)
{
    $ch = [];
    foreach($items as $item)
    {
        $item->curl_init_obj = curl_init($item->link);
    }

    $mh = curl_multi_init();
    foreach($items as $item)
    {
        $c = $item->curl_init_obj;
        curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
        curl_multi_add_handle($mh, $c);
    }

    $running = null;
    do {
        curl_multi_exec($mh, $running);
    } while ($running);

    foreach($items as $item)
    {
        $c = $item->curl_init_obj;
        curl_multi_remove_handle($mh, $c);
    }

    curl_multi_close($mh);

    foreach($items as $item)
    {
        $c = $item->curl_init_obj;
        $output = curl_multi_getcontent($c);
        curl_close($c);
        $item->getStoreLink($output);
    }

    return $items;
}


function multiThread_getStoreContents(array $items)
{
    $ch = [];
    foreach($items as $item)
    {
        $item->curl_init_obj = curl_init($item->link);
    }

    $mh = curl_multi_init();
    foreach($items as $item)
    {
        $c = $item->curl_init_obj;
        curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
        curl_multi_add_handle($mh, $c);
    }

    $running = null;
    do {
        curl_multi_exec($mh, $running);
    } while ($running);

    foreach($items as $item)
    {
        $c = $item->curl_init_obj;
        curl_multi_remove_handle($mh, $c);
    }

    curl_multi_close($mh);

    foreach($items as $item)
    {
        $c = $item->curl_init_obj;
        $output = curl_multi_getcontent($c);
        $item->sortDetails($output);
        curl_close($c);
    }

    return $items;
}