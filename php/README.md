# Etsy Webscraper Script written in PHP

### Script Arguments
| short arg | description |
| :-------------: | ---------------------------------------------------------------------- | 
| -p              | number of etsy result pages to search from                             | 
| -s              | search string. if multiple worlds, enclose in quotes. see sample below | 

----
### Sample Run

```sh
make run-sample
```

----
### Run with Args

```sh
make run -p 5 -s "art prints"
```

----
### 20 pages sample results: 

- results/20_pages/items.csv
- results/20_pages/shops.csv
