<?php

function getElContentsByTagClass($html,$tag,$class)
{
    /* 
    Returns all dom elements of specified tag containing the class value
    */
    $divs = [];
    $doc = new DOMDocument();
    libxml_use_internal_errors(true);
    $doc->loadHTML($html); //Turn the $html string into a DOM document
    $els = $doc->getElementsByTagName($tag); //Find the elements matching our tag name ("div" in this example)
    foreach($els as $el)
    {
        //for each element, get the class, and if it matches return it's contents
        $classAttr = $el->getAttribute("class");
        if(preg_match('#\b'.$class.'\b#',$classAttr) > 0) array_push($divs, DOMinnerHTML($el));
    }
    return $divs;
}
