<?php

function DOMinnerHTML(DOMNode $element)
{
    /*
    Returns Childrend of domNode
    */
    $innerHTML = "";
    $children = $element->childNodes;
    foreach ($children as $child)
    {
        $innerHTML .= $element->ownerDocument->saveHTML($child);
    }
    return $innerHTML;
}