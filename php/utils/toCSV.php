<?php

function toCSV($filename, $data, $method='w')
{
    /**
     * Prints a item or store object to a csv file
     */
    $fp = fopen($filename, $method);

    foreach ($data as $field) 
    {
        fputcsv($fp, $field->toArray());
    }
    
    fclose($fp);
}