<?php

function findMatch($doc, $class, $tag="div", $attr="class")
{
    /*
    Finds the inner DOM of first match of the elemnt with the given class
    */

    $pTags = $doc->getElementsByTagName($tag); 
    foreach($pTags as $p)
    {
        //for each element, get the class, and if it matches return it's contents
        $classAttr = $p->getAttribute($attr);
        if(preg_match('#\b'.$class.'\b#',$classAttr) > 0) 
        {
            return DOMinnerHTML($p);
        }
    }
}