<?php

function findElement($doc, $class, $tag="div", $attr="class")
{
    /*
    returns the dom search
    */

    $pTags = $doc->getElementsByTagName($tag); 
    foreach($pTags as $p)
    {
        //for each element, get the class, and if it matches return it's contents
        $classAttr = $p->getAttribute($attr);
        if(preg_match('#\b'.$class.'\b#',$classAttr) > 0) 
        {
            return $p;
        }
    }
}