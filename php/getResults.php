<?php
require "utils/index.php";
require "helpers/item.class.php";

function getEtsyResults($url)
{
    $results = [];
    $ch = curl_init();
    curl_setopt_array($ch, [
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_URL => $url
    ]);

    // $output contains the output string
    $output = curl_exec($ch);

    // close curl resource to free up system resources
    curl_close($ch); 

    // fetch list item cards
    $elements = getElContentsByTagClass($output,'div','v2-listing-card');

    foreach($elements as $el)
    {
        $item_name = null; 
        $store = null; 
        $stars_average = null; 
        $number_of_raters = null;
        $currency_value = null;
        $link = null;
        $currency_symbol = null;
        $on_sale = null;
        $original_price_value = null;
        
        $doc = new DOMDocument();
        libxml_use_internal_errors(true);
        $doc->loadHTML($el);
        
        // LISTING URL
        $class = "listing-link";
        $url_objcet = $doc->getElementsByTagName("a");
        foreach($url_objcet as $p)
        {
            $classAttr = $p->getAttribute('class');
            if(preg_match('#\b'.$class.'\b#',$classAttr) > 0) 
            {
                $link = $p->getAttribute("href");
                break;
            }
        }
        
        // FIND TITLE
        $h2Tags = $doc->getElementsByTagName("h2");
        $title = DOMinnerHTML($h2Tags[0]);

        // FIND STORE
        $class = "text-gray-lighter text-body-smaller display-inline-block";
        $store_name = findMatch($doc, $class, $tag="p", $attr="class");

        // FIND STARS
        $class = "/stars-svg-star/";
        preg_match_all($class, $el, $stars_match);
        $stars = count($stars_match[0]);

        // FIND NUMBER OF RATINGS
        $class = "text-body-smaller text-gray-lighter display-inline-block icon-b-1";
        $number_of_raters = findMatch($doc, $class, $tag="span", $attr="class");

        // FIND PRICE
        $class_value = "currency-value";
        $class_symbol = "currency-symbol";
        $original_price = null;
        $on_sale = false;
        preg_match_all("/Sale Price/", $el, $sale_price);
        if (count($sale_price[0]) > 0)
        {
            // IS ON SALE
            $on_sale = true;
            $inner_element = findMatch($doc, "Sale Price", $tag="span", $attr="aria-label");
            $doc_inner = new DOMDocument();
            $doc_inner->loadHTML($inner_element);
            $currency_value = findMatch($doc_inner, $class_value, $tag="span", $attr="class");
            $currency_symbol = findMatch($doc_inner, $class_symbol, $tag="span", $attr="class");

            $orig_price_class = "text-body-smaller promotion-price normal no-wrap";
            $orig_price_element = findMatch($doc, $orig_price_class, $tag="span", $attr="class");
            $doc_inner->loadHTML($orig_price_element);
            $original_price = findMatch($doc_inner, $class_value, $tag="span", $attr="class");
        } else 
        {
            // REGULAR PRICE
            $currency_value = findMatch($doc, $class_value, $tag="span", $attr="class");
            $currency_symbol = findMatch($doc, $class_symbol, $tag="span", $attr="class");
        }
        
        array_push($results, new Item(
            $item_name = $title, 
            $store = $store_name, 
            $stars_average = $stars, 
            $number_of_raters = $number_of_raters,
            $currency_value = $currency_value,
            $link = $link,
            $currency_symbol = $currency_symbol,
            $on_sale = $on_sale, 
            $original_price_value = $original_price
        ));
    }
    return $results;
}