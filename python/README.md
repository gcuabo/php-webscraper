# Etsy Webscraper written in Python

### Script Arguments
| long arg | short arg | description |
| ------------- | :-------------: | ---------------------------------------------------------------------- | 
| --pages         | -p              | number of etsy result pages to search from                             | 
| --search_string | -s              | search string. if multiple worlds, enclose in quotes. see sample below | 

----
### Sample Run

```sh
make run-sample
```

----
### Run with args

```sh
make run --pages {pages} --search_string "etsy search string"
```

----
### Sample Results

Sample Results in results/
