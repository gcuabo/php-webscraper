#!/usr/bin/env python
""" DOCSTRING """

import urllib
import argparse
from requests_toolbelt.threaded import pool
from bs4 import BeautifulSoup # pylint: disable=E0401

import utils # pylint: disable=E0401
from models import Item # pylint: disable=E0401

def scrape(pages, search_string, filename=None):
    """ SCRAPE DETAILS FROM SEARCH RESULTS """
    folder_name = utils.create_folder(filename)
    file = open("{}/results.tsv".format(folder_name), "a")

    urls = []
    for page in range(1, pages+1):
        print("FETCHING PAGE {}".format(page))
        urlencoded_params = urllib.parse.urlencode({'q' : search_string,
                                                    'ref' : 'pagination',
                                                    'page': page})
        urls.append("https://www.etsy.com/search?{}".format(urlencoded_params))

    url_pool = pool.Pool.from_urls(urls)
    url_pool.join_all()

    for exc in url_pool.exceptions():
        print('GET {0}. Raised {1}.'.format(exc.request_kwargs['url'],
                                            exc.message))

    for response in url_pool.responses():
        print('GET {0}. Returned {1}.'.format(response.request_kwargs['url'],
                                              response.status_code))
        soup = BeautifulSoup(response.text, 'html.parser')

        listing_urls = [url['href'] for url in soup.find_all(class_='listing-link')]

        listing_pool = pool.Pool.from_urls(listing_urls)
        listing_pool.join_all()

        for p_response in listing_pool.responses():
            print('GET {0}. Returned {1}.'.format(p_response.request_kwargs['url'],
                                                  p_response.status_code))

            result = utils.find_details_in_response(p_response)

            # TODO: FETCH UNIQUE STORE DETAILS FROM RESULTS
            product = Item(title=result['title'],
                           link=result['link'],
                           shop_name=result['shop_name'],
                           shop_link=result['shop_link'],
                           stars=result['stars'],
                           price=result['price'],
                           orig_price=result['orig_price'],
                           number_of_reviewers=result['number_of_reviewers'])

            file.write("{}\n".format(product.to_tsv()))
    file.close()

if __name__ == "__main__":
    PARSER = argparse.ArgumentParser()
    PARSER.add_argument("-p", "--pages", type=int, help="Number of etsy results pages to search")
    PARSER.add_argument("-s", "--search_string", help="etsy search string")
    PARSER.add_argument("-f", "--filename", help="Filename of output file")

    ARGS = PARSER.parse_args()
    scrape(ARGS.pages, ARGS.search_string, ARGS.filename)
