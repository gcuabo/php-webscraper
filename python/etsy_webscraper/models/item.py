""" Class for Item """


class Item:

    """ Item Class """

    # pylint: disable=too-many-instance-attributes
    def __init__(self, title, link, **kwargs):
        self.title = title
        self.link = link
        self.shop_link = kwargs['shop_link'] if kwargs['shop_link'] else None
        self.shop_name = kwargs['shop_name'] if kwargs['shop_name'] else None
        self.stars = kwargs['stars'] if kwargs['stars'] else None
        self.number_of_reviewers = kwargs['number_of_reviewers'] \
                                    if kwargs['number_of_reviewers'] else None
        self.price = kwargs['price'] if kwargs['price'] else None

        orig_price = kwargs['orig_price'] if kwargs['orig_price'] else "N/A"
        self.orig_price = orig_price if orig_price != "Loading" else "N/A"

    def __str__(self):
        return "{} -- {}".format(self.title, self.shop_name)

    def to_tsv(self):
        """ Converts class attributes to csv """

        return "{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}".format(self.title,
                                                       self.link,
                                                       self.shop_name,
                                                       self.shop_link,
                                                       self.price,
                                                       self.orig_price,
                                                       self.stars,
                                                       self.number_of_reviewers)
