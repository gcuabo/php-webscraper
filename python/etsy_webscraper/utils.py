""" Utils """

import os
import re
from datetime import datetime
from bs4 import BeautifulSoup # pylint: disable=E0401

def create_folder(filename=None):
    """ Creates a folder with todays date and current time"""
    if filename:
        folder_name = "results/{}".format(filename)
    else:
        folder_name = "results/{}".format(datetime.now().strftime("%Y-%m-%d_%H%M%S"))
    os.mkdir(folder_name)
    return folder_name

def find_details_in_response(response):
    """ Finds the necessary details from the response """
    soup = BeautifulSoup(response.text, 'html.parser')
    item_doms = soup.find('div', {"id": 'listing-page-cart'})

    soup = BeautifulSoup(str(item_doms), 'html.parser')

    shop_name_soup = soup.find('a', {"class" : re.compile("(.*)text-link-no-underline(.*)")})
    shop_name = shop_name_soup.get_text()
    shop_link = shop_name_soup['href']

    # Find Stars
    stars_soup = soup.find('input', {"name": "initial-rating"})
    stars = stars_soup['value'] if stars_soup else 0

    # Find product name
    product_name_soup = soup.find('h1')
    product_name = product_name_soup.get_text()

    sale_price_soup = soup.find('div', {"data-buy-box-region": "price"})
    price = sale_price_soup.get_text()

    number_of_reviewers_soup = soup.findAll('span', {"class": "wt-screen-reader-only"})
    number_of_reviewers = 0
    for dom in number_of_reviewers_soup:
        if "reviews" in dom.get_text():
            number_of_reviewers = dom.get_text()

    prices = [p.strip()for p in price.split("\n") if p.strip() != '']

    return {
        'title': product_name.strip(),
        'link': response.request_kwargs['url'].strip(),
        'number_of_reviewers': str(number_of_reviewers).strip(),
        'price': prices[0].strip(),
        'orig_price': prices[1].strip() if len(prices) > 1 else None,
        'shop_name': shop_name.strip(),
        'shop_link': shop_link.strip(),
        'stars': stars
    }
